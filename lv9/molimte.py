#Skripta Zadatak_1.py ucitava CIFAR-10 skup podataka. Ovaj skup sadrži 
#50000 slika u skupu za ucenje i 10000 slika za testiranje. Slike su RGB i rezolucije su 32x32. 
#Svakoj slici je pridružena jedna od 10 klasa ovisno koji je objekt prikazan na slici. Potrebno je:
#1. Proucite dostupni kod. Od kojih se slojeva sastoji CNN mreža? Koliko ima parametara ˇ
#mreža?
#2. Pokrenite ucenje mreže. Pratite proces ucenja pomocu alata Tensorboard na sljedeci nacin. 
#Pokrenite Tensorboard u terminalu pomocu naredbe: 
#tensorboard –logdir=logs
#i zatim otvorite adresu http://localhost:6006/ pomocu web preglednika.

import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.utils import to_categorical
from matplotlib import pyplot as plt
#1. Proucite dostupni kod. Od kojih se slojeva sastoji CNN mreža? Koliko ima parametara ˇ
#mreža?

# ucitaj CIFAR-10 podatkovni skup
(X_train, y_train), (X_test, y_test) = cifar10.load_data()

# prikazi 9 slika iz skupa za ucenje
plt.figure()
for i in range(9):
    plt.subplot(330 + 1 + i)
    plt.xticks([])
    plt.yticks([])
    plt.imshow(X_train[i])

plt.show()


# pripremi podatke (skaliraj ih na raspon [0,1]])
X_train_n = X_train.astype('float32')/ 255.0
X_test_n = X_test.astype('float32')/ 255.0

#X_train_n = X_train_n[0:25000,:,:,:] #izdvajanje polovine skupa podataka za ucenje u svrhu zadatka


# 1-od-K kodiranje
#uint8 znaci da je od 0 do 255
y_train = to_categorical(y_train, dtype ="uint8")
y_test = to_categorical(y_test, dtype ="uint8")

# CNN mreza, broj parametara = 1 122 758
model = keras.Sequential()
model.add(layers.Input(shape=(32,32,3)))#3 znaci boja
#konvolucijski sloj, 32 filtera dimenzije 3pua 3 sto je otp standard
model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))#smanjuje dimenziju
model.add(layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Conv2D(filters=128, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Flatten())#transformacija
model.add(layers.Dense(500, activation='relu'))#potpuno povezani sloj
model.add(layers.Dropout(rate=0.3))#za prenapucenost
model.add(layers.Dense(10, activation='softmax'))

model.summary()

# definiraj listu s funkcijama povratnog poziva
#TensorBoard - ova funkcija će pratiti i zapisati razne metrike tijekom treniranja, 
# kao što su gubitak i točnost, kako bi se mogli kasnije pregledati i vizualizirati u TensorBoard alatu.
my_callbacks = [
    keras.callbacks.TensorBoard(log_dir = 'logs/cnn_earlyStop',
                                update_freq = 100),
    #EarlyStopping - ova funkcija će pratiti metriku koja je definirana kao monitor (u ovom slučaju gubitak na validacijskom skupu)
    #  i zaustavit će treniranje kada se ta metrika ne poboljša nakon patience broja epoha(u ovom slucaju 5). 
    keras.callbacks.EarlyStopping(monitor="val_loss", patience=5)
]

#inicijalizaciju objekta optimizer koji će se koristiti za optimizaciju modela.
optimizer = keras.optimizers.Adam() #kontruktoru predati proizvoljni learning_rate, postoji default
#definiraju detalji postupka treniranja modela
model.compile(optimizer=optimizer,
                loss='categorical_crossentropy',
                metrics=['accuracy'])

#za tensorboard, otvoriti novi terminal (New terminal) i upisati tensorboard --logdir logs/cnn(_dropout, _earlyStop) 
#logs-direktorij u koji se sprema, naveden u Callbacku
#otvoriti dani link i prikazani su grafovi

model.fit(X_train_n,
            y_train,
            epochs = 40,
            batch_size = 64,
            callbacks = my_callbacks,
            validation_split = 0.1)

#3. Proucite krivulje koje prikazuju tocnost klasifikacije i prosjecnu vrijednost funkcije gubitka 
#na skupu podataka za ucenje i skupu podataka za validaciju. Što se dogodilo tijekom ucenja 
#mreže? Zapišite tocnost koju ste postigli na skupu podataka za testiranje

#dogodio se overfit, na validacijskom skupu vidljiv je pad točnosti i porast gubitka već nakon prvih 6-7 epoha, 
#dok se na skupu za učenje se sve metrike povećavaju (loss je blizu 0, accuracy 1)

#2. Modificirajte skriptu iz prethodnog zadatka na nacin da na odgovarajuca mjesta u 
#mrežu dodate droput slojeve. Prije pokretanja ucenja promijenite Tensorboard funkciju povratnog 
#poziva na nacin da informacije zapisuje u novi direktorij (npr. =/log/cnn_droput). Pratite tijek
#ucenja. Kako komentirate utjecaj dropout slojeva na performanse mreže?

#nakon primjene dropout sloja, točnost na testnom skupu se bitno povećala, učinak overfita je smanjen (iako je i dalje je 6-7 epoha optimalan broj)

#3. Dodajte funkciju povratnog poziva za rano zaustavljanje koja ce zaustaviti proces ´
#ucenja nakon što se 5 uzastopnih epoha ne smanji prosje ˇ cna vrijednost funkcije gubitka na ˇ
#validacijskom skupu

#earlyStop gleda najmanji prosjecni loss, a ne razliku izmedu lossa 2 susjedne epohe, ako se za 5 ne poboljsa(smanji) najbolji, zaustavlja se

#4 Što se dogada s procesom ucenja: 
#1. ako se koristi jako velika ili jako mala velicina serije? 
#2. ako koristite jako malu ili jako veliku vrijednost stope ucenja? 
#3. ako izbacite odredene slojeve iz mreže kako biste dobili manju mrežu? 
#4. ako za 50% smanjite velicinu skupa za ucenje? 

#jako velika velicina batcha - manje iteracija, krace trajanje epohe, losija tocnost i veci loss 
#jako mala velicina batcha - vise iteracija, duze trajanje epohe (predugo)
#jako mala vrijednost stope ucenja - ucenje izrazito sporo konvergira, loss jedva pada, accuracy jedva raste 
#jako velika vrijednost stope ucenja - loss velik, accuracy mali, i ne mijenjaju se 
#izbacivanje slojeva iz mreze za manju mrezu - izbacivanjem jednog dense, conv2d i maxpooling sloja, epoha traje krace, losiji accuracy i loss veci
#50% manja velicina skupa za ucenje - upola manje iteracija, veci loss i losiji accuracy, epoha traje krace

score = model.evaluate(X_test_n, y_test, verbose=0)
print(f'Tocnost na testnom skupu podataka: {100.0*score[1]:.2f}%')
#Dobivena točnost nakon 40 epoha na testnom skupu = 73.27%
#Dobivena točnost nakon 20 epoha na testnom skupu uz dropout sloj izmedu 2 potpuno povezana sloja uz rate 0.3 = 76.04%
#Dobivena točnost nakon 40 epoha na testnom skupu uz dropout sloj izmedu 2 potpuno povezana sloja uz rate 0.3 i earlyStop(patience=5) = 75.86%
#Stane nakon 11. epohe zbog earlyStoppinga

