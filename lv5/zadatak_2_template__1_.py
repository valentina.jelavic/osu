#Skripta zadatak_2.py ucitava podatkovni skup Palmer Penguins [1]. Ovaj 
#podatkovni skup sadrži mjerenja provedena na tri razlicite vrste pingvina (’Adelie’, ’Chins-
#trap’, ’Gentoo’) na tri razlicita otoka u podrucju Palmer Station, Antarktika. Vrsta pingvina 
#odabrana je kao izlazna velicina i pri tome su klase oznacene s cjelobrojnim vrijednostima 
#0, 1 i 2. Ulazne velicine su duljina kljuna (’bill_length_mm’) i duljina peraje u mm (’flipper_length_mm’).
#Za vizualizaciju podatkovnih primjera i granice odluke u skripti je dostupna
#funkcija plot_decision_region.

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.model_selection import train_test_split
from sklearn . linear_model import LogisticRegression
from sklearn . metrics import accuracy_score
from sklearn . metrics import confusion_matrix , ConfusionMatrixDisplay
from sklearn.metrics import classification_report

labels= {0:'Adelie', 1:'Chinstrap', 2:'Gentoo'}

#vizualizacija granice odluke za 2 ulazne varijable
#Veća vrijednost resolution znači da će se korak mreže smanjiti, što će rezultirati točnijom granicom, ali i većim vremenom izvođenja
def plot_decision_regions(X, y, classifier, resolution=0.02):
    plt.figure()
    # setup marker generator and color map
    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    #broj boja u mapi je određen duljinom skupa jedinstvenih vrijednosti u vektoru "y".
    #koriste samo prvih "len(np.unique(y))" elemenata
    #np.unique(y) vraća jedinstvene vrijednosti iz niza y;Ovo je korisno jer broj klasa može biti različit u ovisnosti o skupu podataka koji se koristi, pa će se u tom slučaju automatski generirati lista boja odgovarajuće duljine.
    cmap = ListedColormap(colors[:len(np.unique(y))])
    
    # plot the decision surface
    #granica odluke
    #raspon vrijednosti koordinata na dvodimenzionalnoj ravnini na kojoj će se crtati granica odluke klasifikator
    # minimum i maksimum za prvi stupac u varijablama x1_min i x1_max,
    #za minus plus jedan:Time se osigurava da svi primjeri iz skupa podataka budu vidljivi na grafu, čak i ako su smješteni na rubu domene podataka.
    #primjeti da X ima dva stupca-to je u zad
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    #druga linija to čini za drugu koordinatu (stupac) u varijablama x2_min i x2_max
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1

    #np.meshgrid se koristi da bi se definirale točke koordinatne mreže s kojima se stvara ravnina granice odluke
    #prima 3 argumenta:x1, x2, udaljenost tocaka medusobno-sto je manje gusce je
    #x1_min = 0, x1_max = 2, x2_min = 1, x2_max = 3 i resolution = 0.5, 
    #tada bi np.arange(x1_min, x1_max, resolution) generirao niz [0, 0.5, 1, 1.5]- jer na MAX vrije NE UTJECE ressolution
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution), np.arange(x2_min, x2_max, resolution))

#Funkcija ravel koristi se za "ravnjanje" matrice koja se prosljeđuje klasifikatoru kako bi se dobila jednodimenzionalna matrica odgovora.
#Nakon što se dobije matrica odgovora, funkcijom reshape se ponovno oblikuje u matricu istih dimenzija kao što su dimenzije mreže xx1.
#Ovo omogućava da se na kraju primijeni funkcija contourf kako bi se nacrtala granica odluke koja razdvaja različite klase u području koje pokriva mreža.
    #NE RAZUMIJEM?????? 
    #ravel() se koristi kako bi se ova dva retka spojila u jedan ravninski numpy array, čime se dobiva oblik (n_samples, 2)
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)# kreira dvodimenzionalni numpy array u kojem se prvi redak sastoji od vrijednosti prve koordinate (x1) za sve točke u ravnini, a drugi redak sadrži vrijednosti druge koordinate (x2) za sve točke u ravnini
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.3, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())
    
    # plot class examples
    #enumetare se koristi za iteriranje kroz sve jednistvene vrijednosti y
    for idx, cl in enumerate(np.unique(y)):
        #x: uzima sve retke iz prvog stupca za koje odgovara u skupu oznaka y jednaka je klasi cl
        #idx broji indekse klasa
        plt.scatter(x=X[y == cl, 0],
                    y=X[y == cl, 1],
                    alpha=0.8,
                    c=colors[idx],
                    marker=markers[idx],
                    edgecolor = 'w',
                    label=labels[cl])
    plt.show()





# ucitaj podatke
df = pd.read_csv("penguins.csv")

# izostale vrijednosti po stupcima
print(df.isnull().sum())

# spol ima 11 izostalih vrijednosti; izbacit cemo ovaj stupac
df = df.drop(columns=['sex'])

# obrisi redove s izostalim vrijednostima
df.dropna(axis=0, inplace=True)

# kategoricka varijabla vrsta - kodiranje
df['species'].replace({'Adelie' : 0,
                        'Chinstrap' : 1,
                        'Gentoo': 2}, inplace = True)

print(df.info())

# izlazna velicina: species
output_variable = ['species']

# ulazne velicine: bill length, flipper_length-pise u zadatku tocno
input_variables = ['bill_length_mm',
                    'flipper_length_mm']

X = df[input_variables].to_numpy()
#on je dodao ovo u zadnjim uglatim
y = df[output_variable].to_numpy()[:,0]#Ako se ne koristi [:,0], onda bi y bio dvodimenzionalni numpy array sa samo jednom kolonom(provjeri sa shape)

# podjela train/test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 123)

#a) Pomocu stupcastog dijagrama prikažite koliko primjera postoji za svaku klasu (vrstu 
#pingvina) u skupu podataka za ucenje i skupu podataka za testiranje. Koristite numpy 
#funkciju unique
#ne radi bez return counts: pogreska je ValueError: too many values to unpack (expected 2)
#return_counts=True is an optional parameter in the numpy.unique() function that returns the count of each unique element in the array. It returns a tuple where the first element is an array of unique elements and the second element is an array of the count of each unique element in the input array.
#vrsta pingvina je y
#classes predstavlja jedinstvene vrijednosti u varijabli y_train i y_test.
#count_train i count_test su nizovi koji sadrže broj pojavljivanja svake klase
classes, count_train=np.unique(y_train, return_counts=True)
classes, count_test=np.unique(y_test, return_counts=True)
# ne valjaaaa  plt.bar(df, count_train, color ='maroon', width = 0.4), ValueError: shape mismatch: objects cannot be broadcast to a single shape.  Mismatch is between arg 0 with shape (342, 6) and arg 1 with shape (3,).
#kreiranje niza indeksa
X_axis = np.arange(len(classes))
#ide minus i plus 0.2 da se ne poklapaju
plt.bar(X_axis - 0.2, count_train, 0.4, label = 'Train')
plt.bar(X_axis + 0.2, count_test, 0.4, label = 'Test')
plt.xticks(X_axis, ['Adelie(0)', 'Chinstrap(1)', 'Gentoo(2)'])
plt.xlabel("Penguins")
plt.ylabel("Counts")
plt.title("Number of each class of penguins, train and test data")
plt.legend() 
plt.show()

#b) Izgradite model logisticke regresije pomocu scikit-learn biblioteke na temelju skupa poda-
#taka za ucenje.
# n_iter_i = _check_optimize_result(- napisi 
LogRegression_model = LogisticRegression (max_iter=120)
LogRegression_model . fit ( X_train , y_train )

#c) Pronadite u atributima izgradenog modela parametre modela. Koja je razlika u odnosu na 
#binarni klasifikacijski problem iz prvog zadatka?
bias=LogRegression_model.intercept_ #teta0, coef vraca samo parametre uz ulazne velicine (za linearnu isto! (lv4))
coefs=LogRegression_model.coef_  # theta1 i theta2.
print(bias)
#prva dim:broj klasa, druga: broj ulaznih velicina
print(coefs.shape)# ima 3 retka i 2 stupca,zbog broja klasa (3), ima 3 ,svaki redak za jednu klasu
#jer OvR radi binarnih klasifikatora koliko ima klasa)(1xk dimenzije), kod binarne klasifikacije je bio 1 element(1 klasa)
#kod binarne klasifikacije je bio 1 red s 2 stupca (1 binarni klasifikator, 2 ulazne velicine)
#zbog 3 klase, ima 3 retka parametara, svaki redak za jednu klasu, i 2 stupca, svaki stupac u paru s jednom ulaznom velicinom (kXm dimenzije)

#d) Pozovite funkciju plot_decision_region pri cemu joj predajte podatke za ucenje i 
#izgradeni model logisticke regresije. Kako komentirate dobivene rezultate?
plot_decision_regions(X_train, y_train, LogRegression_model )

#e) Provedite klasifikaciju skupa podataka za testiranje pomocu izgradenog modela logisticke 
#regresije. Izracunajte i prikažite matricu zabune na testnim podacima. Izracunajte tocnost. 
#Pomocu classification_report funkcije izracunajte vrijednost cetiri glavne metrike 
#na skupu podataka za testiranje.
y_test_p = LogRegression_model . predict ( X_test )

cm = confusion_matrix ( y_test , y_test_p )
print (" Matrica zabune : " , cm )
disp = ConfusionMatrixDisplay ( confusion_matrix ( y_test , y_test_p ) )
disp . plot ()
plt . show ()
print(f'Točnost: {accuracy_score(y_test,y_test_p)}')
print ( classification_report ( y_test , y_test_p ) )


#f) Dodajte u model još ulaznih velicina. Što se dogada s rezultatima klasifikacije na skupu
#podataka za testiranje?
#???????????????????????????????????????
#dodavanjem parametra body mass, kvaliteta modela opada, dodavanjem bill_depth poraste, kao i dodavanjem oba parametra
