#Skripta zadatak_1.py generira umjetni binarni klasifikacijski problem
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn . linear_model import LogisticRegression
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn . metrics import accuracy_score, recall_score,precision_score
from sklearn . metrics import confusion_matrix , ConfusionMatrixDisplay


#ona, koristila je random state jer ne koristimo database
#n_samples - ukupni broj uzoraka koje će funkcija generirati
#n_features - ukupni broj značajki koje će funkcija generirati
#n_informative - broj značajki koje su informativne, odnosno korisne za klasifikaciju
#n_redundant - broj redundatnih značajki, koje su linearne kombinacije informativnih značajki
#random_state - postavlja sjeme za generiranje slučajnih brojeva, što omogućava reproducibilnost eksperimenata
#n_clusters_per_class - broj klastera po klasi, tj. koliko će različitih skupova podataka biti u svakoj klasi
#class_sep - kontrolira koliko će se klase preklapati, odnosno koliko će biti teško klasificirati pojedine uzorke
X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
# ona
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

#a) Prikažite podatke za ucenje u x1−x2 ravnini matplotlib biblioteke pri cemu podatke obojite 
#s obzirom na klasu. Prikažite i podatke iz skupa za testiranje, ali za njih koristite drugi
#marker (npr. ’x’). Koristite funkciju scatter koja osim podataka prima i parametre c i
#cmap kojima je moguce definirati boju svake klase.
#X1 X2 RAVNINA SE ODNOSI NA ROWS(JER IMAMO 2 RETKA)
colors=['blue', 'red']
#the [:,0] and [:,1] are used to select all rows (denoted by :), but only the first and second columns respectively.
#n_informative=2, zato ima dva iz X!!!!!
# da smo imali 3 stupca imali bi 3d prostor: ax.scatter3D(X_train[:,0], X_train[:,1], X_train[:,2], c=y_train, cmap=matplotlib.colors.ListedColormap(colors))
plt.scatter(X_train[:,0], X_train[:,1], c=y_train, cmap=matplotlib.colors.ListedColormap(colors)) #redom, 0 blue, 1 red
plt.scatter(X_test[:,0], X_test[:,1], c=y_test, marker="x", cmap=matplotlib.colors.ListedColormap(colors))
plt.xlabel('x1')
plt.ylabel('x2')
plt.title("Podaci za treniranje(.) i testiranje(x)")
cbar=plt.colorbar(ticks=[0,1])
plt.show()

#b) Izgradite model logisticke regresije pomocu scikit-learn biblioteke na temelju skupa poda-
#taka za ucenje
LogRegression_model = LogisticRegression()
LogRegression_model . fit ( X_train , y_train )


#c) Pronadite u atributima izgradenog modela parametre modela. 
# Prikažite granicu odluke naucenog modela u ravnini x1 − x2 zajedno s podacima za ucenje. 
# Napomena: granica odluke u ravnini x1 −x2 definirana je kao krivulja: θ0 +θ1x1 +θ2x2 = 0.
#nisam znala-jjjjjjj
bias=LogRegression_model.intercept_ #teta0, coef vraca samo parametre uz ulazne velicine (za linearnu isto! (lv4))
coefs=LogRegression_model.coef_  # theta1 i theta2.
print(coefs.shape)#ima 1 redak i dva stupca
a = -coefs[0,0]/coefs[0,1]#koeficijent nagiba pravca granice odluke., inace je: a = -w[0] / w[1],ali ovdje ima 2 stupca, ako je 1: samo prvu brojku ignoriraj
c = -bias/coefs[0,1]#presjek pravca granice odluke s osi x2., ide bias/coef u 1.stupcu

#ovo nam treba za pravac jer su dovoljne dvije tocke- ali imamo samo x vrijednosti zasad(y=..izracunava se)
x1x2min = X_train.min().min()-0.5#X_train.min() vraća minimum vrijednosti u matrici X_train. No, X_train je matrica dimenzija (n_samples, n_features), stoga X_train.min() vraća najmanju vrijednost u svim 200 uzoraka za svih 2 značajke.
#x1x2min i x1x2max predstavljaju minimalne i maksimalne vrijednosti u skupu podataka, s time da se na svaku stranu dodaje vrijednost od 0.5 kako bi granica odluke bila vidljivija na dijagramu
#Kada bismo imali skup podataka X_train koji sadrži 3 značajke, tada bismo koristili X_train.min(axis=0).min() kako bismo dobili minimalnu vrijednost za svaku značajku.
#a ako je 1 stupac :X_train.max() + 0.5
x1x2max = X_train.max().max()+0.5
xd = np.array([x1x2min, x1x2max]) #za pravac dovoljne dvije tocke,xd je niz od minimalne i maksimalne vrijednosti x1 koje se koriste za crtanje pravca granice odluke.
yd = a*xd + c#niz izračunat kao y-koordinate pravca granice odluke za svaku vrijednost x1 u xd.(klasicna formula pravca)
plt.plot(xd, yd, linestyle='--')
# obojale površine ispod i iznad granice odluke različitim bojama-prvi slucaj je kad je manje
plt.fill_between(xd, yd, x1x2min, color='red', alpha=0.2) #1
plt.fill_between(xd, yd, x1x2max, color='blue', alpha=0.2) #0
plt.scatter(X_train[:,0], X_train[:,1], c=y_train, cmap=matplotlib.colors.ListedColormap(colors), edgecolor="white")
plt.xlim(x1x2min, x1x2max)
plt.ylim(x1x2min, x1x2max)
plt.xlabel('x1')
plt.ylabel('x2')
plt.title('Podaci za treniranje i granica odluke')
cbar=plt.colorbar(ticks=[0,1])
plt.show()


#d) Provedite klasifikaciju skupa podataka za testiranje pomocu izgradenog modela logisticke 
#regresije. Izracunajte i prikažite matricu zabune na testnim podacima. Izracunate tocnost, 
#preciznost i odziv na skupu podataka za testiranje.
y_test_p = LogRegression_model . predict ( X_test )
# tocnost
print(f'Točnost: {accuracy_score(y_test,y_test_p)}')
print(f'Preciznost: {precision_score(y_test,y_test_p)}')
print(f'Odziv: {recall_score(y_test,y_test_p)}')
# matrica zabune
cm = confusion_matrix ( y_test , y_test_p )
print (" Matrica zabune : " , cm )
disp = ConfusionMatrixDisplay ( confusion_matrix ( y_test , y_test_p ) )
disp . plot ()
plt . show ()
# report
print ( recall_score ( y_test , y_test_p ) )



#e) Prikažite skup za testiranje u ravnini x1 −x2. Zelenom bojom oznacite dobro klasificirane
#primjere dok pogrešno klasificirane primjere oznacite crnom bojom.
colorss=['black', 'green']
#jjjj
plt.scatter(X_test[:,0], X_test[:,1], c=y_test==y_test_p, cmap=matplotlib.colors.ListedColormap(colorss)) #redom, false black, true green,u cmap prvi element je false,a uvijek je true ili false
plt.xlabel('x1')
plt.ylabel('x2')
plt.title("Dobro  i pogresno klasificirani ")
cbar=plt.colorbar(ticks=[0,1])
cbar.ax.set_yticklabels(['Netočno','Točno'])
plt.show()



