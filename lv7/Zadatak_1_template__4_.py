#Skripta zadatak_1.py sadrži funkciju generate_data koja služi za generiranje
#umjetnih podatkovnih primjera kako bi se demonstriralo grupiranje. Funkcija prima cijeli broj
#koji definira željeni broju uzoraka u skupu i cijeli broj od 1 do 5 koji definira na koji nacin ce
#se generirati podaci, a vraca generirani skup podataka u obliku numpy polja pri cemu su prvi i 
#drugi stupac vrijednosti prve odnosno druge ulazne velicine za svaki podatak. Skripta generira 
#500 podatkovnih primjera i prikazuje ih u obliku dijagrama raspršenja.

import matplotlib.pyplot as plt
import numpy as np
from scipy.cluster.hierarchy import dendrogram
from sklearn.datasets import make_blobs, make_circles, make_moons
from sklearn.cluster import KMeans, AgglomerativeClustering

#n_samples odreduje ukuoni broj primjera u skupu podataka-po zadatku je 2
#flagc odreduje vrstu skupa podataka koji se generira
def generate_data(n_samples, flagc):

    #tri skupine u kojima su podaci međusobno udaljeni i ne preklapaju se.
    #jednostavni skup podataka
    # 3 grupe
    if flagc == 1:
        random_state = 365
        #make_blobs je za generiranje skupa podataka koji sadrzi 3 skupine
        #n_samples odreduje broj podataka koji ce biti generiran
        #random_state je sjeme slucajnosti za generiranje skupa podataka
        #X su znacajke, a y oznake klasa
        #cijela funk vraca X
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
    

    # kada želimo generirati skup podataka s tri skupine u kojima su podaci međusobno povezani linearnom transformacijom
   # za slozenije strukture u kojima podaci nisu jednostavno odvojeni
    # 3 grupe
    #osim prve vrste radi i ovo:
    #rimjenjuje linearna transformacija na podatke kako bi se dobila drugačija struktura podataka
    elif flagc == 2:
        random_state = 148
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
        #primjenjuje se linearna transformacija na podatke koja se definira matricom transformation
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        #množenjem matrice X koja sadrži originalne podatke s matricom transformacije transformation pomoću np.dot funkcije
        X = np.dot(X, transformation)

    # 4 grupe 
    #koristimo kada  želimo kontrolirati varijabilnost podataka unutar svake grupe. 
    elif flagc == 3:
        random_state = 148
        X, y = make_blobs(n_samples=n_samples,
                        #postoji 4 centra u kojima se grupe nalaze
                        centers = 4,
                        #standardna devijacija varijacije podataka za svaku od tih grupa definirana u cluster_std kao niz [1.0, 2.5, 0.5, 3.0].
                        #da bi se kontrolirala varijabilnost podataka unutar svake grupe(Ako su vrijednosti podataka u grupi vrlo različite, to će utjecati na kvalitetu gručenja)
                        cluster_std=np.array([1.0, 2.5, 0.5, 3.0]),
                        random_state=random_state)
   
   
   
    #2 grupe, opet dva tipa generiranja:
    # ako se radi o problemu klasifikacije u kojem su klase jasno razdvojene kružnim oblikom, tada bi funkcija make_circles() bila korisna. S druge strane,
    # ako su klase raspoređene u obliku međusobno isprepletenih polukružnih skupina, tada bi bilo korisnije koristiti funkciju make_moons().
    # 2 grupe
    elif flagc == 4:
        #factor određuje veličinu unutarnjeg kruga u odnosu na vanjski krug
        #noise određuje razinu nasumičnog šuma u podacima.
        X, y = make_circles(n_samples=n_samples, factor=.5, noise=.05)
    
    # 2 grupe  
    elif flagc == 5:
        #make _moon-va funkcija generira dvije međusobno isprepletene polukružne skupine 
        X, y = make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

# generiranje podatkovnih primjera-500
X = generate_data(500, 1)

# prikazi primjere u obliku dijagrama rasprsenja
plt.figure()
plt.scatter(X[:,0],X[:,1])#pise 2 ulazne velicine
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('podatkovni primjeri')
plt.show()

#1. Pokrenite skriptu. Prepoznajete li koliko ima grupa u generiranim podacima? Mijenjajte
#nacin generiranja podataka
#ima 3 grupe
#ako stavim da je drugi parametar  4 dobijem 2 tipa podatak u kruznicama jedno u drugoj
#ako stavim da je drugi parametar  5 dobijem 2 tipa podatak u u shapeovima 


#2. Primijenite metodu K srednjih vrijednosti te ponovo prikažite primjere, ali svaki primjer
#obojite ovisno o njegovoj pripadnosti pojedinoj grupi. Nekoliko puta pokrenite programski
#kod. Mijenjate broj K. Što primjecujete?

# inicijalizacija algoritma K srednjih vrijednosti
#n_clusters – broj grupa
#init – nacini inicijalizacije centara 
#n_init – koliko puta ce se algoritam izvršiti s razlicitim pocetnim centrima, a konacan rezultat su centri koji su postigli najmanju vrijednost kriterijske funkcije
km = KMeans ( n_clusters =3, init ='random',n_init =5 , random_state =0 )
# pokretanje grupiranja primjera
km . fit ( X )
labels = km . predict ( X )
plt.figure()
#jj je dodao c pa nadalje do kraja, jer pise da moramo obojiti primjere ovisno o gurpi
plt.scatter(X[:,0],X[:,1], c=labels, cmap='viridis')
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('podatkovni primjeri')
plt.show()

#kada mijenjamo k, nepravilno se rasporeduju podaci(nor ako je k 5 a generira se 3 pod primjera )


#3. Mijenjajte nacin definiranja umjetnih primjera te promatrajte rezultate grupiranja podataka
#(koristite optimalni broj grupa). Kako komentirate dobivene rezultate?
#kmeaans radi na principu udaljenosti, pa ako stavimo param da je npr 4.(one kruznice) nece dobor podijeliti podatke
#kada flagc=1, radi dobro jer su grupe sfericne



