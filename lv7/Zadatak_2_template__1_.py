#Kvantizacija boje je proces smanjivanja broja razlicitih boja u digitalnoj slici, ali 
#uzimajuci u obzir da rezultantna slika vizualno bude što slicnija originalnoj slici. 
#Jednostavan nacin kvantizacije boje može se postici primjenom algoritma K srednjih vrijednosti na RGB
#vrijednosti elemenata originalne slike.
# Kvantizacija se tada postiže zamjenom vrijednosti svakog elementa originalne slike s njemu najbližim centrom.
# Na slici 7.3a dan je primjer originalne
#slike koja sadrži ukupno 106,276 boja, dok je na slici 7.3b prikazana rezultantna slika nakon
#kvantizacije i koja sadrži samo 5 boja koje su odredene algoritmom K srednjih vrijednosti.

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("imgs\\test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
#imshow je za slike
plt.imshow(img)
# je funkcija iz biblioteke Matplotlib koja automatski podešava razmake između područja crteža kako bi se osiguralo
#  da se svi elementi (npr. osi, oznake, naslovi) koji se nalaze unutar crteža nalaze na odgovarajućem mjestu i 
# da se ne preklapaju. 
plt.tight_layout()
plt.show()

#pretvori vrijednosti elemenata slike u raspon 0 do 1
#pretvara tip podataka u float64 i zatim dijeli sve vrijednosti piksela
#s maksimalnom vrijednosti 255 kako bi se svi pikseli nalazili u rasponu između 0 i 1.
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
#w*h ukupan broj piksela slike(visina puta sirina), a d broj kanala slike
w,h,d = img.shape
#np.reshape() metoda zatim transformira img niz u oblik (w*h, d)-to se koristi za grupiranje piksela
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
#tvara kopiju img_array vektora podataka slike
#koristi za primjenu metode smanjenja dimenzionalnosti i dobivanje aproksimirane verzije originalne slike.
img_array_aprox = img_array.copy()


#1. Otvorite skriptu zadatak_2.py. Ova skripta ucitava originalnu RGB sliku test_1.jpg
#te ju transformira u podatkovni skup koji dimenzijama odgovara izrazu (7.2) pri cemu je n
#broj elemenata slike, a m je jednak 3. Koliko je razlicitih boja prisutno u ovoj slici? 
#nemoj napisati ovo jer trebam uniqueee!!!
#print(len(img_array_aprox))
#ulazni niz je 2D polje (dimenzija (w*h) x d), pa ako ne navedete "axis=0", tada se traže jedinstvene vrijednosti za cijeli 2D niz,
#  što može dovesti do drugačijeg rezultata u odnosu na kada tražite jedinstvene vrijednosti po svakom stupcu 
# (dimenziji 0) u polju
print(f'Broj boja u originalnoj slici: {len(np.unique(img_array_aprox, axis=0))}')

#2. Primijenite algoritam K srednjih vrijednosti koji ce pronaci grupe u RGB vrijednostima 
#elemenata originalne slike.

#jj ima samo nclusters i init(i u 1.pogledaj!!)
km = KMeans ( n_clusters =5, init ='random',n_init =5 , random_state =0 )
km . fit ( img_array_aprox )
labels = km . predict ( img_array_aprox )


#3. Vrijednost svakog elementa slike originalne slike zamijeni s njemu pripadajucim centrom.
#jjjjj

for i in range(len(labels)):
    #iako pise originalne slike, koristimo ovu novi skaliranu
    img_array_aprox[i]=km.cluster_centers_[labels[i]] 
    #promjena svakog retka(boje) u jednu od k boja (najblizi i odgovarajuci centroid) - kvantizacija slike
print(f'Broj boja u aproksimiranoj slici: {len(np.unique(img_array_aprox, axis=0))}')

#4. Usporedite dobivenu sliku s originalnom. Mijenjate broj grupa K. Komentirajte dobivene
#rezultate.

#ako sada pokrenemo dobijemo pravac kao y os i neke brojeve na njemu
#zato moramo povratiti u originalne dimenzije
img_aprox = np.reshape(img_array_aprox, (w,h,d))    #povratak na originalnu dimenziju slike 
img_aprox = (img_aprox*255).astype(np.uint8)        #povratak iz raspona 0 do 1 u int
plt.figure()
plt.title("Aproksimirana slika")
plt.imshow(img_aprox)
plt.tight_layout()
plt.show()
#sto je manji k, to je losija slika i sve vise odudara od originalne
#povecavanjem broja grupa, vise je boja, slika sve vise izgleda kao originalna, smanjivanjem manje boja i sve dalje od originala
#duljina izvodenja programa se bitno povecava s povecanjem broja grupa


#7. Elemente slike koji pripadaju jednoj grupi prikažite kao zasebnu binarnu sliku. Što
#primjecujete
#jjjjjjjjjjjjjjjjjjjj
#svaka grupa boja je predstavljena binarnom slikom koja sadrži bijele piksele gdje 
# se nalaze pikseli koji pripadaju toj grupi boja, a crne piksele tamo gdje se nalaze pikseli koji ne pripadaju toj grupi boja.
#jedinstveni labeli koji se nalaze u labelima
labels_unique = np.unique(labels)
#u petlji prolazi kroz sve jedinstvene labele i kreira se binarna slika koja sadrži vrijednost True 
# tamo gdje se labeli u labelima podudaraju s trenutnim jedinstvenim labelom, a False tamo gdje se ne podudaraju.
for i in range(len(labels_unique)):
    #kreira binarnu sliku za i-tu grupu boja
    binary_image = labels==labels_unique[i] #labels je n_pixela x 1 shape
    #binarna slika reshape-a kako bi se vratila u originalne dimenzije slike (bez RGB dimenzije)
    binary_image = np.reshape(binary_image, (w,h)) #potrebno reshapeat za prikaz nazad u normalne dimenzije slike(bez rgb dimenzije)
    plt.figure()
    plt.title(f"Binarna slika {i+1}. grupe boja")
    plt.imshow(binary_image)
    plt.tight_layout()
    plt.show()
#prikazom binarnih slika svake grupe, primjecuje se da su grupe disjunktni skupovi, tj. svaka grupa predstavlja jednu boju na slici
#imam 8 slika jer sam stavila na 8 grupa



#5. Primijenite postupak i na ostale dostupne slike.
#----------------------------------------------------------------------2.SLIKA----------------
imgD = Image.imread("imgs\\test_2.jpg")

plt.figure()
plt.title("Originalna slika")
plt.imshow(imgD) 
plt.tight_layout()
plt.show()

imgD = imgD.astype(np.float64) / 255
wD,hD,dD = imgD.shape
img_arrayD = np.reshape(imgD, (wD*hD, dD))
img_array_aproxD = img_arrayD.copy()
print(f'Broj boja u originalnoj slici: {len(np.unique(img_array_aproxD, axis=0))}')

kmD = KMeans ( n_clusters =5, init ='random',n_init =5 , random_state =0)
kmD . fit ( img_array_aproxD )
labelsD = kmD . predict ( img_array_aproxD )

for i in range(len(labelsD)):
    img_array_aproxD[i]=kmD.cluster_centers_[labelsD[i]] 

print(f'Broj boja u aproksimiranoj slici: {len(np.unique(img_array_aproxD, axis=0))}')

img_aproxD = np.reshape(img_array_aproxD, (wD,hD,dD))    #povratak na originalnu dimenziju slike 
img_aproxD = (img_aproxD*255).astype(np.uint8)        #povratak iz raspona 0 do 1 u int
plt.figure()
plt.title("Aproksimirana slika")
plt.imshow(img_aproxD)
plt.tight_layout()
plt.show()

#6. Graficki prikažite ovisnost J o broju grupa K. Koristite atribut inertia objekta klase
#KMeans. Možete li uociti lakat koji upucuje na optimalni broj grupa?
#jjj to nije napravio
#ovo je s gpta
#dobije se pogreska  Number of distinct clusters (5) found smaller than n_clusters (7). to je zbog duplikata
#NE VALJAAAA PITAJ NEKOG
imgT = Image.imread("imgs\\test_3.jpg")

plt.figure()
plt.title("Originalna slika")
plt.imshow(imgT) 
plt.tight_layout()
plt.show()

imgT = imgT.astype(np.float64) / 255
wt,ht,dt = imgT.shape
img_arrayt = np.reshape(imgT, (wt*ht, dt))
img_array_aproxt = img_arrayt.copy()
img_array_aproxt = np.unique(img_array_aproxt, n_clusters=5)
inertias = []
ks = range(6, 10)
for k in ks:
    km = KMeans(n_clusters=k, init='random', n_init=5, random_state=0)
    img_array_km = km.fit_transform(img_array_aproxt)
    inertia = np.sum(img_array_km ** 2)
    inertias.append(inertia)

plt.plot(ks, inertias, '-o')
plt.xlabel('Broj grupa (K)')
plt.ylabel('Inercija')
plt.title('Ovisnost inercije o broju grupa (K)')
plt.xticks(ks)
plt.show()
#NE VALJA


#-------------------------------------------------------------zavrseno-------------------------------------
