#1
import numpy as np
import matplotlib.pyplot as plt


x = np.array([1, 2, 3, 3, 1])
y = np.array([1, 2, 2, 1, 1])

plt.plot(x, y, 'r', linewidth=3, marker="o", markersize=10)
plt.axis([0.0, 4.0, 0.0, 4.0])
plt.xlabel("x-os")
plt.ylabel("y-os")
plt.title("Primjer")
plt.show()


#2
import numpy as np
import matplotlib.pyplot as plt


data = np.loadtxt('data.csv', delimiter=',', skiprows=1)

print(f"Broj ljudi: {data.shape[0]}")

visina = data[:, 1]
masa = data[:, 2]

plt.scatter(visina, masa, c='b', s=1, marker=".")
plt.xlabel('Visina(cm)')
plt.ylabel('Masa(kg)')
plt.title('Odnos visine i mase')
plt.show()

visina50 = data[::50, 1]
masa50 = data[::50, 2]
plt.scatter(visina50, masa50, c='b', s=1, marker=".")
plt.xlabel('Visina(cm)')
plt.ylabel('Masa(kg)')
plt.title('Odnos visine i mase (svaka 50-a osoba)')
plt.show()

print(f'Najveca vrijednost visine: {visina.max()}cm')
print(f'Najmanja vrijednost visine: {visina.min()}cm')
print(f'Srednja vrijednost visine: {visina.mean()}cm')

visinaM = data[:, 1][data[:, 0] == 1]
visinaZ = data[:, 1][data[:, 0] == 0]

print(f'Najveca vrijednost visine muskaraca: {visinaM.max()}cm')
print(f'Najmanja vrijednost visine muskaraca: {visinaM.min()}cm')
print(f'Srednja vrijednost visine muskaraca: {visinaM.mean()}cm')

print(f'Najveca vrijednost visine zena: {visinaZ.max()}cm')
print(f'Najmanja vrijednost visine zena : {visinaZ.min()}cm')
print(f'Srednja vrijednost visine zena: {visinaZ.mean()}cm')




#3
import numpy as np
import matplotlib.pyplot as plt

img = plt.imread('road.jpg')
img = img[:, :, 0].copy()
plt.imshow(img, cmap="gray")
plt.title("Cesta")
plt.show()

lighterImg = img+150
lighterImg[lighterImg < 150] = 255
plt.imshow(lighterImg, cmap="gray")
plt.title("Light cesta")
plt.show()

quarterImg = img[:, int(img.shape[1]/4):int(img.shape[1]/2)]
plt.imshow(quarterImg, cmap="gray")
plt.title("Sliceana cesta")
plt.show()

rotatedImg = np.rot90(img, 3)
plt.imshow(rotatedImg, cmap="gray")
plt.title("Cesta zarotirana za 90")
plt.show()

mirrorImg = np.flip(img, axis=1)
plt.imshow(mirrorImg, cmap="gray")
plt.title("Zrcaljena cesta")
plt.show()


#4
import numpy as np
import matplotlib.pyplot as plt

firstLight = np.ones((50, 50))*255
secondLight = firstLight.copy()
firstDark = np.zeros((50, 50))
secondDark = firstDark.copy()

firstRow = np.hstack((firstDark, firstLight))
secondRow = np.hstack((secondLight, secondDark))
fullSquare = np.vstack((firstRow, secondRow))
plt.imshow(fullSquare, cmap="gray")
plt.title("Crno bijeli kvadrat")
plt.show()


