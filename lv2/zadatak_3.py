#Skripta zadatak_3.py ucitava sliku ’road.jpg’. Manipulacijom odgovarajuce
#numpy matrice pokušajte:
#a) posvijetliti sliku,
#b) prikazati samo drugu cetvrtinu slike po širini,
#c) zarotirati sliku za 90 stupnjeva u smjeru kazaljke na satu,
#d) zrcaliti sliku.

import numpy as np
import matplotlib.pyplot as plt

#za ucitavanje slike koristi se imread
img = plt.imread("road.jpg")
#od 3 mogucnosti biramo 1 jer je gray
img = img [:,:,0].copy()
plt . figure()
plt . imshow( img , cmap ="gray")
plt . show()


#NISTA NISAM ZNALA
#a
#Adds 150 to every pixel value in the img array
brightImg=img+150
#Sets any pixel value in brightImg that is less than 150 to 255- ako to ne stavim i dalje imam tamne tonove
brightImg[brightImg<150]=255
plt.imshow(brightImg, cmap="gray")
plt.show()

#b
#svi retci
#i stupci od 1/4 do polovine jer je to 2/4
#cijela visina, ali cetvrtina sirine
#da ide po retcima pisalo bi : shape[0]
quarterImg = img[:, int(img.shape[1]/4):int(img.shape[1]/2)]
plt.imshow(quarterImg, cmap="gray")
plt.show()

#b) prikazati samo drugu cetvrtinu slike po duzini
quarterduzzsImg=img[int(img.shape[0]/4) :int(img.shape[0]/2), :]
plt.imshow(quarterduzzsImg, cmap="gray")
plt.show()


#ovo je moj primjer za 1r 1s
#funkcionira na nacin da odredimo sredisnji element(int(img.shape[0]/2)) i uzimamo prije ili poslije njega sto zelimo
v=img[:int(img.shape[0]/2), :int(img.shape[1]/2)]
plt.imshow(v, cmap="gray")
plt.show()


#prikazati samo trecu cetvrtinu slike po duzini
quarterssImg=img[int(img.shape[0]/2) :int(3*img.shape[0]/4), :]
plt.imshow(quarterssImg, cmap="gray")
plt.show()


#c
#k odreduje za koliko se pomice
rotate_img = np.rot90(img, k=3)
plt.imshow(rotate_img, cmap="gray")
plt.show()

#d
#axis utjece na kako se flipa( po x ili y osi, ako je 1 onda y)
flipped_img=np.flip(img,axis=1 )
plt.imshow(flipped_img, cmap="gray")
plt.show()

