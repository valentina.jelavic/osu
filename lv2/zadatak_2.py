#Datoteka data.csv sadrži mjerenja visine i mase provedena na muškarcima i
#ženama. Skripta zadatak_2.py ucitava dane podatke u obliku numpy polja data pri cemu je u 
#prvom stupcu polja oznaka spola (1 muško, 0 žensko), drugi stupac polja je visina u cm, a treci
#stupac polja je masa u kg.
import numpy as np
import matplotlib . pyplot as plt

#a) Na temelju velicine numpy polja data, na koliko osoba su izvršena mjerenja?

#jjj NISAM ZNALA
#ucitava podatke u obliku numpy polja
#delimeter znaci da su podaci odvojeni zarezom, a skiprows preskace prvi redak(jer je to kao zaglavlje)
data = np.loadtxt('data.csv', delimiter=',', skiprows=1)
print('Mjerenja su izvrsena na:',len(data[:,1]),'osoba')

#b)Prikažite odnos visine i mase osobe pomocu naredbe matplotlib.pyplot.scatter.
x=data[:,1]
y=data[:,2]
#s je velicina tocaka
plt.scatter(x,y,s=1, marker=".")
plt.xlabel('Visina(cm)')
plt.ylabel('Masa(kg)')
plt.title('Odnos visine i mase')
plt.show()

#c) Ponovite prethodni zadatak, ali prikažite mjerenja za svaku pedesetu osobu na slici.
#::50 jer je step zadnji parametar, to znaci sve retke ali svaki 50
x=data[::50,1]
y=data[::50,2]
plt.scatter(x,y,s=1, marker=".")
plt.xlabel('Visina(cm)')
plt.ylabel('Masa(kg)')
plt.title('Odnos visine i mase (svaka 50-a osoba)')
plt.show()


#d) Izracunajte i ispišite u terminal minimalnu, maksimalnu i srednju vrijednost visine u ovom 
#podatkovnom skupu
print('min', min(data[:,1]))# mogla sam i x.max()
print('max', max(data[:,1]))
print('mean', np.mean(data[:,1]))



#e) Ponovite zadatak pod d), ali samo za muškarce, odnosno žene. Npr. kako biste izdvojili
#muškarce, stvorite polje koje zadrži bool vrijednosti i njega koristite kao indeks retka.
#ind = (data[:,0] == 1)

ind = (data[:,0] == 1)# ovo dosl mozes direktno umjesto ind
print('min', min(data[ind,1]))
print('max', max(data[ind,1]))
print('mean', np.mean(data[ind,1]))

indz = (data[:,0] == 0)
print('min', min(data[indz,1]))
print('max', max(data[indz,1]))
print('mean', np.mean(data[indz,1]))

#visinaM = data[:, 1][data[:, 0] == 1] //1 stupac ako je 0.ti stupac jednak 1 ili 0
#visinaZ = data[:, 1][data[:, 0] == 0]
#onda moze pisati visinaM.max()








