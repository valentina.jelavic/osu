#Pomocu funkcija  numpy.array i matplotlib.pyplot pokušajte nacrtati sliku
#2.3 u okviru skripte zadatak_1.py. Igrajte se sa slikom, promijenite boju linija, debljinu linije i
#sl.
import numpy as np
import matplotlib . pyplot as plt

plt.axis([0.0, 4.0, 0.0, 4.0])
plt.plot([1.0, 3.0], [1.0, 1.0], c='b',linewidth =1.5, marker='o')
plt.plot([3.0, 3.0], [1.0 , 2.0],c='b',linewidth =1.5,marker='o')
plt.plot([3.0,2.0], [2.0,2.0],c='b',linewidth =1.5,marker='o')
plt.plot([2.0,1.0], [2.0,1.0],c='b',linewidth =1.5,marker='o')
plt.show()

#mogla sam i ovako:
#ides redom tockama i pises x kordinate u x i tako za y
x = np.array([1,3,3,2,1])
y = np.array([1,1,2,2,1])
plt.plot(x, y, 'r', linewidth=3, marker="o", markersize=10)
plt.axis([0.0, 4.0, 0.0, 4.0])
plt.xlabel("x-os")
plt.ylabel("y-os")
plt.title("Primjer")
plt.show()
