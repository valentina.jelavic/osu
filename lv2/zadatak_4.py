#Napišite program koji ce kreirati sliku koja sadrži cetiri kvadrata crne odnosno 
#bijele boje (vidi primjer slike 2.4 ispod). Za kreiranje ove funkcije koristite numpy funkcije
#zeros i ones kako biste kreirali crna i bijela polja dimenzija 50x50 piksela. Kako biste ih složili
#u odgovarajuci oblik koristite numpy funkcije hstack i vstack

import numpy as np
import matplotlib.pyplot as plt

#3 utjece na boju--ali sam ga obrisala jer mogu dodati ca+map gray pa mi bude crno bijelo
#posto moram 100 puta 100 sliku, gledaj koliko male slike iznose-to vidis na slici(ovaj put 50X50)
img = np.zeros((50, 50))
#mnozis s 255 da dobijem bijelu boju
img2 = np.ones((50, 50))
img2 = 255*img2

#horizontalno zlaze
first_h=np.hstack((img, img2))
second_h=np.hstack((img2, img))
#vertikalno slaze
final=np.vstack((first_h, second_h))
plt.imshow(final,cmap="gray")
plt.show()
