#Na temelju rješenja prethodnog zadatka izradite model koji koristi i kategoricku 
#varijable „Fuel Type“ kao ulaznu velicinu. Pri tome koristite 1-od-K kodiranje kategorickih 
#velicina. Radi jednostavnosti nemojte skalirati ulazne velicine. Komentirajte dobivene rezultate. 
#Kolika je maksimalna pogreška u procjeni emisije C02 plinova u g/km? O kojem se modelu
#vozila radi?
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import max_error
from sklearn.compose import ColumnTransformer
import matplotlib.pyplot as plt
from sklearn . preprocessing import MinMaxScaler
import sklearn . linear_model as lm
from sklearn . preprocessing import OneHotEncoder


data = pd.read_csv('data_C02_emission.csv')


ohe=OneHotEncoder()
fuelTypeEncoded=ohe.fit_transform(data[['Fuel Type']]).toarray() #OneHotEncoder.fit_transform ocekuje 2d array(dataframe[[stupac(i)]]), ne moze 1d (series[stupac])
# nove numeričke varijable dodaju u originalni DataFrame pomoću .categories_ metode koja vraća kategorije varijable
#  (u ovom slučaju samo "Fuel Type") kao listu lista
# Ovdje se koristi [0] kako bi se izvukla jedina lista unutar ove liste kategorija, tj. lista "Fuel Type"
data[ohe.categories_[0]]=fuelTypeEncoded #kategorije su imena stupaca, pohranjene u listu lista kategorija atributa categories_, zato [0], tj. jedina lista kategorija iz prosle linije dobivena
output_variable='CO2 Emissions (g/km)'
y=data[output_variable].copy()#kao i  kod numerickih ide copy
X = data.drop('CO2 Emissions (g/km)', axis=1)

#potrebne sve velicine iz dataframe za laksi pronalazak modela kasnije
X_train_all , X_test_all , y_train , y_test = train_test_split (X, y, test_size = 0.2, random_state =1)

#izdvajanje numerickih velicina-primjeti da pise i x, z, ...sto je nekad bio string, morat ces printat u colabu
input_variables=['Engine Size (L)','Cylinders','Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Consumption Comb (mpg)','D', 'E', 'X', 'Z']
X_train = X_train_all[input_variables]
X_test = X_test_all[input_variables]

#klasicna linearna regresija...
linearModel = lm.LinearRegression()
linearModel.fit(X_train,y_train)
y_prediction = linearModel.predict(X_test)
plt.scatter(X_test['Fuel Consumption City (L/100km)'],y_test, c='b',label='Real values', s=5)
plt.scatter(X_test['Fuel Consumption City (L/100km)'],y_prediction, c='r',label='Prediction', s=5)
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.legend()
plt.show()


#najveca greska je gdje je najveca razkika izmedu stvarne i predvidene vrijednosti
maxError = max_error(y_test,y_prediction)
print(f"Model vozila s najvecom greskom u predvidanju: {X_test_all[abs(y_test-y_prediction)==maxError]['Model'].iloc[0]}")







