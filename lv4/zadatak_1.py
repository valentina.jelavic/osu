#Zadatak 4.5.1 Skripta zadatak_1.py ucitava podatkovni skup iz data_C02_emission.csv.
#Potrebno je izgraditi i vrednovati model koji procjenjuje emisiju C02 plinova na temelju ostalih numerickih ulaznih
#velicina. Detalje oko ovog podatkovnog skupa mogu se pronaci u 3.laboratorijskoj vježbi.
import numpy as np
import pandas as pd
from sklearn . model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn . preprocessing import MinMaxScaler
import sklearn . linear_model as lm
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_percentage_error
from sklearn.metrics import r2_score

data = pd.read_csv('data_C02_emission.csv')

#a) Odaberite željene numericke velicine specificiranjem liste s nazivima stupaca. Podijelite
#podatke na skup za ucenje i skup za testiranje u omjeru 80%-20%.
#za y ide copy, a za X ne jer su to samo znacajke
#PAZIIIII SAMO NUMERICKKEEEEEEEEEE, pise u zad
X = data[['Engine Size (L)','Cylinders','Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Consumption Comb (mpg)']]
y = data['CO2 Emissions (g/km)'].copy()
X_train , X_test , y_train , y_test = train_test_split (X , y , test_size = 0.2 , random_state =1 )

#b) Pomocu matplotlib biblioteke i dijagrama raspršenja prikažite ovisnost emisije C02 plinova 
#o jednoj numerickoj velicini. Pri tome podatke koji pripadaju skupu za ucenje oznacite 
#plavom bojom, a podatke koji pripadaju skupu za testiranje oznacite crvenom bojom.

#nisam znala sama, doslovno je mislila svakii posebno
#jjj je prikazao svaki col sa svakim??
#jos pripazi na to koja je y, jer tu ne uzimas za x
plt.scatter( X_train['Engine Size (L)'],y_train,c='b', label='Train')
plt.scatter( X_test['Engine Size (L)'],y_test,c='r', label='Test')
plt.title('Ovisnost emisije CO2 plinova i engine sizeu- train i test podaci')
plt.show()

#ako zelis sa svima:
#for col in X_train.columns:
    #plt.scatter(X_train[col],y_train, c='b', label='Train', s=5)
   # plt.scatter(X_test[col],y_test, c='r', label='Test', s=5)
   # plt.xlabel(col)
   # plt.ylabel('CO2 Emissions (g/km)')
   # plt.legend()
   # plt.show()



#c) Izvršite standardizaciju ulaznih velicina skupa za ucenje. Prikažite histogram vrijednosti 
#jedne ulazne velicine prije i nakon skaliranja. Na temelju dobivenih parametara skaliranja 
#transformirajte ulazne velicine skupa podataka za testiranje
sc = MinMaxScaler()
#JJJ-pisamo mi je ovo: ValueError: could not convert string to float: 'Ford',MORALA SAM DODATI PD.DATAFRAME
#transform vraca numpy array, zato se mora nazad u dataframe (da se sve radilo s numpy, ne bi bilo potrebe za time)
#ovi param colums = i index= osiguravaju da nazivi ostanu isti
X_train_n = pd.DataFrame(sc.fit_transform(X_train),columns=X_train.columns, index=X_train.index)
X_test_n = pd.DataFrame(sc.fit_transform(X_test),columns=X_test.columns, index=X_test.index)
#jjj je opet sve
plt.subplot(1,2,1)
plt.hist(X_train['Engine Size (L)'])
plt.subplot(1,2,2)
plt.hist(X_train_n['Engine Size (L)'])
plt.title('Ulazna velicina prije i nakon skaliranja')
plt.tight_layout() 
plt.show()

#mozes i sve
#for col in X_train.columns:
  #  fig,axs = plt.subplots(2,figsize=(8, 8))
  #  axs[0].hist(X_train[col])
  #  axs[0].set_title('Before scaler')
  #  axs[1].hist(X_train_n[col])
  #  axs[1].set_xlabel(col)
 #   axs[1].set_title('After scaler')
 #   plt.show()


#d) Izgradite linearni regresijski modeli. Ispišite u terminal dobivene parametre modela i
#povežite ih s izrazom 4.6
linearModel = lm.LinearRegression()
linearModel.fit(X_train_n , y_train)
print(linearModel.coef_)
#on je i intercept? linearModel.intercept_

#e)Izvršite procjenu izlazne velicine na temelju ulaznih velicina skupa za testiranje. Prikažite 
#pomocu dijagrama raspršenja odnos izmedu stvarnih vrijednosti izlazne velicine i procjene 
#dobivene modelom
y_test_p = linearModel.predict(X_test_n)

#jjj je stavio ono u uglatim
#ako ne stavim dobivam gresku:ValueError: x and y must be the same size -zato uzimam jedan stupac
#paziii stalno sam dobila tu gresku jer sam imala x train, a testiram test za sve
plt.scatter(X_test_n['Fuel Consumption City (L/100km)'],y_test, c='b', label='Real',s=5)
plt.scatter( X_test_n['Fuel Consumption City (L/100km)'],y_test_p, c='r', label='Predicted',s=5)
plt.title('odnos izmedu stvarnih vrijednosti izlazne velicine i procjene')
plt.show()

#g) Što se dogada s vrijednostima evaluacijskih metrika na testnom skupu kada mijenjate broj 
#ulaznih velicina?
#kako mislis mijenjamo broj ulaznih velicina??
#prepisala jer mi se nije dalo
print(f'Mean squared error: {mean_squared_error(y_test, y_test_p)}')
print(f'Mean absolute error: {mean_absolute_error(y_test, y_test_p)}')
print(f'Mean absolute percentage error: {mean_absolute_percentage_error(y_test, y_test_p)}%')
print(f'R2 score: {r2_score(y_test, y_test_p)}')




