#Skripta zadatak_1.py ucitava Social_Network_Ads.csv skup podataka [2].
#Ovaj skup sadrži podatke o korisnicima koji jesu ili nisu napravili kupovinu za prikazani oglas.
#Podaci o korisnicima su spol, dob i procijenjena placa. Razmatra se binarni klasifikacijski
#problem gdje su dob i procijenjena placa ulazne velicine, dok je kupovina (0 ili 1) izlazna 
#velicina. Za vizualizaciju podatkovnih primjera i granice odluke u skripti je dostupna funkcija 
#plot_decision_region [1]. Podaci su podijeljeni na skup za ucenje i skup za testiranje modela 
#u omjeru 80%-20% te su standardizirani. Izgraden je model logisticke regresije te je izracunata 
#njegova tocnost na skupu podataka za ucenje i skupu podataka za testiranje. Potrebno je: 

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm

from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV
from sklearn . model_selection import cross_val_score


def plot_decision_regions(X, y, classifier, resolution=0.02):
    plt.figure()
    # setup marker generator and color map
    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])
    
    # plot the decision surface
    #ono u lv5 sto ne kuzim
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.3, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())
    
    # plot class examples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0],
                    y=X[y == cl, 1],
                    alpha=0.8,
                    c=colors[idx],
                    marker=markers[idx],
                    label=cl)


# ucitaj podatke
data = pd.read_csv("Social_Network_Ads.csv")
print(data.info())# ima 2 floata, 2 insta i 1 object

data.hist()# ne prikazuje sex, jer je to string
plt.show()

# dataframe u numpy-sve zadano u zad
X = data[["Age","EstimatedSalary"]].to_numpy()
y = data["Purchased"].to_numpy()

# podijeli podatke u omjeru 80-20%
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, stratify=y, random_state = 10)

# skaliraj ulazne velicine-MORA SE PRIJE ALG KNN, nema toga u lv5
sc = StandardScaler()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform((X_test))

# Model logisticke regresije-penalty je None: da ne primjenjujemo regularizaciju na koeficijente modela
LogReg_model = LogisticRegression(penalty=None) 
LogReg_model.fit(X_train_n, y_train)

# Evaluacija modela logisticke regresije-TO JE PREDICT
y_train_p = LogReg_model.predict(X_train_n)
y_test_p = LogReg_model.predict(X_test_n)

print("Logisticka regresija: ")
print("Tocnost train: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p))))
print("Tocnost test: " + "{:0.3f}".format((accuracy_score(y_test, y_test_p))))

# granica odluke pomocu logisticke regresije
plot_decision_regions(X_train_n, y_train, classifier=LogReg_model)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("Tocnost: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p))))
plt.tight_layout()
plt.show()


#1.Izradite algoritam KNN na skupu podataka za ucenje (uz K=5). Izracunajte tocnost 
#klasifikacije na skupu podataka za ucenje i skupu podataka za testiranje. Usporedite 
#dobivene rezultate s rezultatima logisticke regresije. Što primjecujete vezano uz dobivenu 
#granicu odluke KNN modela?

# inicijalizacija i ucenje KNN modela
KNN_model = KNeighborsClassifier ( n_neighbors = 5 )
KNN_model . fit ( X_train_n , y_train )

# predikcija na skupu podataka za testiranje
y_test_p_KNN = KNN_model . predict ( X_test_n )
y_train_p_KNN = KNN_model.predict(X_train_n)

print("KNN: ")
print("Tocnost train: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p_KNN))))
print("Tocnost test: " + "{:0.3f}".format((accuracy_score(y_test, y_test_p_KNN))))

# granica odluke pomocu logisticke regresije
plot_decision_regions(X_train_n, y_train, classifier=KNN_model)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("Tocnost: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p_KNN))))
plt.tight_layout()
plt.show()
##puno bolje od logisticke regresije
#granica odluke knn, primjecuje se bolja prilagodenost podacima (nelinearna) u odnosu na logisticku regresiju


#2. Kako izgleda granica odluke kada je K =1 i kada je K = 100?
# inicijalizacija i ucenje KNN modela
KNN_model_1 = KNeighborsClassifier ( n_neighbors = 1)
KNN_model_1 . fit ( X_train_n , y_train )
y_test_p_KNN_1 = KNN_model_1 . predict ( X_test_n )
y_train_p_KNN_1 = KNN_model_1.predict(X_train_n)
print("KNN: ")
print("Tocnost train: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p_KNN_1))))
print("Tocnost test: " + "{:0.3f}".format((accuracy_score(y_test, y_test_p_KNN_1))))
plot_decision_regions(X_train_n, y_train, classifier=KNN_model_1)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("Tocnost za K=1: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p_KNN_1))))
plt.tight_layout()
plt.show()


KNN_model_100 = KNeighborsClassifier ( n_neighbors = 100)
KNN_model_100 . fit ( X_train_n , y_train )
y_test_p_KNN_100 = KNN_model_100 . predict ( X_test_n )
y_train_p_KNN_100 = KNN_model_100.predict(X_train_n)
print("KNN: ")
print("Tocnost train: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p_KNN_100))))
print("Tocnost test: " + "{:0.3f}".format((accuracy_score(y_test, y_test_p_KNN_100))))
plot_decision_regions(X_train_n, y_train, classifier=KNN_model_100)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("Tocnost za K=100: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p_KNN_100))))
plt.tight_layout()
plt.show()
#granica odluke kada je K=1 overfit, K=100 underfit, vrlo komplicirana kada je K=1, jednostavnija kada je K=100
#jjjj je greska

#2. zadatak
# Pomocu unakrsne validacije odredite optimalnu vrijednost hiperparametra  K
#algoritma KNN za podatke iz Zadatka 1.
scores = cross_val_score ( KNN_model , X_train_n , y_train , cv =5 )
print ( scores )


#3. zadatak
#  Na podatke iz Zadatka 1 primijenite SVM model koji koristi RBF kernel funkciju
#te prikažite dobivenu granicu odluke. Mijenjajte vrijednost hiperparametra C i γ. Kako promjena
#ovih hiperparametara utjece na granicu odluke te pogrešku na skupu podataka za testiranje? ˇ
#Mijenjajte tip kernela koji se koristi. Što primjecujete?
Covi = [1, 100]
gammas=[1, 100]
for C in Covi:
    for gamma in gammas:
        SVM_model = svm . SVC(kernel='rbf',gamma=gamma, C=C)
        SVM_model . fit ( X_train_n , y_train )

        y_test_p_SVM = SVM_model . predict ( X_test_n )
        y_train_p_SVM = SVM_model . predict ( X_train_n )
        plot_decision_regions(X_train_n, y_train, classifier=SVM_model)
        plt.xlabel('x_1')
        plt.ylabel('x_2')
        plt.legend(loc='upper left')
        plt.title(f"Tocnost za model s K={C} i gamma ={gamma}: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p_SVM))))
        plt.tight_layout()
        plt.show()
#smanjivanjem C i gamme, dogada se underfit, povecanjem overfit, gamma je obicno oko 1, i C oko 1 za zadovoljavajucu tocnost
#promjenom kernela za iste parametre rezultati su drugaciji i granica odluke se bitno mijenja


#4. zadatak
#Pomocu unakrsne validacije odredite optimalnu vrijednost hiperparametra  C i γ
#algoritma SVM za problem iz Zadatka 1.
#posto moram 2 parametra moram koristit gridsearch
param_grid = {'C': [1,100], 'gamma': [1,100]}
#ne pipe kao prvi arg nego sm-SVC()
svm_gscv = GridSearchCV( svm.SVC(), param_grid , cv =5 , scoring ='accuracy' )
svm_gscv . fit ( X_train_n , y_train )
print('Optimalni parametri')
print ( svm_gscv . best_params_ )
print('Prosjecna tocnost')
print ( svm_gscv . best_score_ )
print('rezultati gridsearcha')
#zbog nekog razloga morala sam dodati pd.dataframe
print ( pd.DataFrame(svm_gscv . cv_results_ ))



