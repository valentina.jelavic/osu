#Skripta zadatak_1.py ucitava podatkovni skup iz data_C02_emission.csv.
#Dodajte programski kod u skriptu pomocu kojeg možete odgovoriti na sljedeca pitanja: 

#a) Koliko mjerenja sadrži DataFrame? Kojeg je tipa svaka velicina? Postoje li izostale ili 
#duplicirane vrijednosti? Obrišite ih ako postoje. Kategoricke velicine konvertirajte u tip 
#category.

import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

print('Broj mjerenja(redaka) u data je ', len(data))
print('Tipovi velicina:', data.dtypes)
print('Izostale vrijednosti\n', data.isnull().sum())
print('Ima li duplikata',data.duplicated().any())
#nema ni dupliciranih ni izostalih pa necu izvrsit ovo dolje
#data.dropna()
#print('Izostale vrijednosti\n', data.isnull().sum())
#data.duplicated().sum()

#ona je make vehicle trans i fuel
data["Make"] = pd.Categorical(data["Make"])
data["Model"] = pd.Categorical(data["Model"])
data["Vehicle Class"] = pd.Categorical(data["Vehicle Class"])
data["Transmission"] = pd.Categorical(data["Transmission"])
data["Fuel Type"] = pd.Categorical(data["Fuel Type"])
print(data.info()) #ili print(data.dtypes)

#b) Koja tri automobila ima najvecu odnosno najmanju gradsku potrošnju? Ispišite u terminal: 
#ime proizvodaca, model vozila i kolika je gradska potrošnja.
#ascending je true ako ide od najmanjeg
mind=data.sort_values(by=['Fuel Consumption City (L/100km)'], ascending=True) 
print('Automobili s najmanjom gradskom potrosnjom:\n', mind.iloc[:,[0,1,7]].head(3))

maxd=data.sort_values(by=['Fuel Consumption City (L/100km)'], ascending=False) 
print('Automobili s najvecom gradskom potrosnjom:\n', maxd.iloc[:,[0,1,7]].head(3))


#c) Koliko vozila ima velicinu motora izmedu 2.5 i 3.5 L? Kolika je prosjecna C02 emisija 
#plinova za ova vozila?
#moras nap data jos jednu jer trazis duzinu data, a ne true false jer je to svih 2221 elementa
print('Broj vozila koji imaju velicinu motora izmedu 2.5 i 3.5 je',len(data[(data['Engine Size (L)']>=2.5) & (data['Engine Size (L)']<=3.5)]))
print('Prosjecna emisija CO2 plinova za ta vozila je:', data[(data['Engine Size (L)']>2.5) & (data['Engine Size (L)']<3.5)]['CO2 Emissions (g/km)'].mean())
#print(data[(data['Engine Size (L)']>=2.5) & (data['Engine Size (L)']<=3.5)].agg({'Model':'count', 'CO2 Emissions (g/km)':'mean'}))

#d) Koliko mjerenja se odnosi na vozila proizvodaca Audi? Kolika je prosjecna emisija C02 
#plinova automobila proizvodaca Audi koji imaju 4 cilindara?
print('Broj mjerenja za vozila Audi proizvodaca:', len(data[(data['Make']=="Audi")]))
print('Prosjecna emisija plinova za Audije koji imaju 4 cilindra je:', data[(data['Make']=="Audi") & (data['Cylinders']==4)]['CO2 Emissions (g/km)'].mean())

#e) Koliko je vozila s 4,6,8. . . cilindara? Kolika je prosjecna emisija C02 plinova s obzirom na 
#broj cilindara?
dataCyl=data.groupby('Cylinders')
print(dataCyl.count())
print(data.groupby('Cylinders').mean('CO2 Emissions (g/km)'))
#print(data.groupby('Cylinders').agg({'Make':'count', 'CO2 Emissions (g/km)':'mean'}).rename(columns={'Make': 'Car count'}))

#f) Kolika je prosjecna gradska potrošnja u slucaju vozila koja koriste dizel, a kolika za vozila 
#koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?
print('Prosjecna potrosnja u vozila koje koriste dizel je:', data[(data['Fuel Type']=="D")]["Fuel Consumption City (L/100km)"].mean())
print('Prosjecna potrosnja u vozila koje koriste regularni benzin je:', data[(data['Fuel Type']=="X")]["Fuel Consumption City (L/100km)"].mean())

print('Medijalne vrijednosti za dizel:',data[(data['Fuel Type']=="D")]["Fuel Consumption City (L/100km)"].median() )
print('Medijalne vrijednosti za regularni benzin:',data[(data['Fuel Type']=="X")]["Fuel Consumption City (L/100km)"].median() )

#g) Koje vozilo s 4 cilindra koje koristi dizelski motor ima najvecu gradsku potrošnju goriva?
#JJJJJ 
#uocavam kada trebamo najvece najmanje.. koristim sort_values
print('Vozilo s 4 cilindra dizel s najvećom potrošnjom u gradu:')
print(data[(data['Cylinders']==4) & (data['Fuel Type']=='D')].sort_values(by='Fuel Consumption City (L/100km)').tail(1))

#h) Koliko ima vozila ima rucni tip mjenjaca (bez obzira na broj brzina)?
#pazi na startswith, contains itd, mora ici .str.
print('Broj vozila sa rucnim tipom mjenjaca je :', len(data[(data['Transmission'].str.startswith("M"))]['Model']))

#i) Izracunajte korelaciju izmedu numerickih velicina. Komentirajte dobiveni rezultat.
print(data.corr(numeric_only = True))
#Korelacija je mjera veze ili odnosa između dvije varijable.
# Ona nam govori koliko se dvije varijable kreću zajedno ili suprotno jedna od druge.
# Korelacija se izračunava kao koeficijent korelacije, koji je vrijednost između -1 i 1.
# Korelacijski koeficijent u tom slučaju će biti pozitivan i bliži vrijednosti 1 što će ukazivati na jaku pozitivnu korelaciju.
#S druge strane, kada su dvije varijable negativno korelirane, to znači da postoji tendencija da se povećanje vrijednosti jedne varijable podudara smanjenjem vrijednosti druge varijable i obrnuto. Korelacijski koeficijent u tom slučaju će biti negativan i bliži vrijednosti -1 što će ukazivati na jaku negativnu korelaciju.
#Kada dvije varijable nisu korelirane, njihova vrijednost korelacijskog koeficijenta će biti 0.

#Fuel Consumption City (L/100km) ima pozitivnu korelaciju s Fuel Consumption Hwy (L/100km), a negativnu s Fuel Consumption Comb (mpg)





