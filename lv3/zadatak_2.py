#Zadatak 3.4.2 Napišite programski kod koji ce prikazati sljedece vizualizacije: 
import pandas as pd
import matplotlib . pyplot as plt

data = pd.read_csv('data_C02_emission.csv')

#a) Pomocu histograma prikažite emisiju C02 plinova. Komentirajte dobiveni prikaz.
plt.figure()
data['CO2 Emissions (g/km)'].plot(kind ='hist', bins = 20 )
plt.show()

#b) Pomocu dijagrama raspršenja prikažite odnos izmedu gradske potrošnje goriva i emisije 
#C02 plinova. Komentirajte dobiveni prikaz. Kako biste bolje razumjeli odnose izmedu
#velicina, obojite tockice na dijagramu raspršenja s obzirom na tip goriva.

#stalno dobivam gresku:for colormapping provided via 'c'. Parameters 'cmap' will be ignored ,scatter = ax.scatter( 
#jjjjj je ovako:
#sve kategoricke velicine moramo staviti- on nije model?, isto se dobije
data['Make']=pd.Categorical(data['Make'])
data["Model"] = pd.Categorical(data["Model"])
data['Vehicle Class']=pd.Categorical(data['Vehicle Class'])
data['Transmission']=pd.Categorical(data['Transmission'])
data['Fuel Type']=pd.Categorical(data['Fuel Type'])

#on je dodao fuel type jer pise da se boja s obzirom na to!
data.plot.scatter( x='Fuel Consumption City (L/100km)' , y='CO2 Emissions (g/km)',c='Fuel Type', cmap ="viridis", s=20 )
plt.show()


#c) Pomocu kutijastog dijagrama prikažite razdiobu izvangradske potrošnje s obzirom na tip 
#goriva. Primjecujete li grubu mjernu pogrešku u podacima?
grouped = data.groupby('Fuel Type')
grouped.boxplot(column = ['Fuel Consumption Hwy (L/100km)'])
plt.show()
#Za Z, ima mnogo outliera


#d) Pomocu stupcastog dijagrama prikažite broj vozila po tipu goriva. Koristite metodu groupby
#jjjjjj-nisam imala pojma-greske stalno
#ako stavim ili model ili make bude isto
#agg jer moramo brojati koliko je toga u make
data.groupby('Fuel Type').agg({'Make':'count'}).rename(columns={'Make':'Car number'}).plot(kind="bar")
plt.show()

#e) Pomocu stupcastog grafa prikažite na istoj slici prosjecnu C02 emisiju vozila s obzirom na broj cilindara.
#razl!
data.groupby('Cylinders').agg({'CO2 Emissions (g/km)':'mean'}).plot(kind="bar")
plt.show()








