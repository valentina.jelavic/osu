#Napišite program koji od korisnika zahtijeva upis jednog broja koji predstavlja
#nekakvu ocjenu i nalazi se izmedu 0.0 i 1.0. Ispišite kojoj kategoriji pripada ocjena na temelju 
#sljedecih uvjeta: 
#>= 0.9 A
#>= 0.8 B
#>= 0.7 C
#>= 0.6 D
#< 0.6 F
#Ako korisnik nije utipkao broj, ispišite na ekran poruku o grešci (koristite try i except naredbe).
#Takoder, ako je broj izvan intervala [0.0 i 1.0] potrebno je ispisati odgovaraju  cu poruku



print('Unesite jedan broj izmedu 0.0 i 1.0')
try:
    broj=float(input())
except:
    print('Nisi unio poruku')
    exit(1)
while broj < 0.0 or broj > 1.0:
    try:
        print('Broj nije u zadanom rasponu, unesite sljedeci:')
        broj=float(input())
    except:
        print("Nisi unio poruku")
        exit(1)

if broj >= 0.9:
    print('A')
elif broj >= 0.8:
    print('B')
elif broj >= 0.7:
    print('C')
elif broj >= 0.6:
    print('D')
elif broj < 0.6:
    print('F')

#mogla sam samo dodati da se ispise ako nije u rasponu u one ifove
#if number < 0.0 or number > 1.0:
#  print("Broj nije izmedu 0 i 1")