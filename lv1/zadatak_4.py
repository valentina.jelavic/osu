#Napišite Python skriptu koja ce u  citati tekstualnu datoteku naziva song.txt.
#Potrebno je napraviti rjecnik koji kao kljuceve koristi sve razlicite rijeci koje se pojavljuju u 
#datoteci, dok su vrijednosti jednake broju puta koliko se svaka rijec (kljuc) pojavljuje u datoteci. 
#Koliko je rijeci koje se pojavljuju samo jednom u datoteci? Ispišite ih.

data = open("song.txt", "r")
dict={}
for line in data:
       #removes any leading (spaces at the beginning) and trailing (spaces at the end) characters (space is the default leading character to remove)
       #jj je koristio rstrip
       line = line.strip()
       #ne moram koristiti ako su sve malim slovom
       line = line.lower()
       #pretvara string u listu, moze biti bez parametara, isto se dobije
       #TO JE LINIJA KODA
       words = line.split(" ")

        #RIJEC PO RIJEC U liniji kodaaa
       for word in words:
        #jj jos ima ovo, ali u song.txt nema zareza word = word.rstrip(',')
        # Check if the word is already in dictionary
        if word in dict:
            #DICT[WORD] je vrijednost po kljucu word
            dict[word] = dict[word] + 1
        else:
            dict[word] = 1
  
for key in list(dict.keys()):
    print(key, ":", dict[key])
       
    
counter = 0
for key in list(dict.keys()):
    if(dict[key]==1):
        counter=counter+1
        print(key)

print('Broj rijeci koje su se ponovile samo jednom je: ', counter)
