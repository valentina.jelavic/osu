#Napišite program koji od korisnika zahtijeva unos brojeva u beskonacnoj petlji 
#sve dok korisnik ne upiše „Done“ (bez navodnika). Pri tome brojeve spremajte u listu. Nakon toga
#potrebno je ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i maksimalnu
#vrijednost. Sortirajte listu i ispišite je na ekran. Dodatno: osigurajte program od pogrešnog unosa
#(npr. slovo umjesto brojke) na nacin da program zanemari taj unos i ispiše odgovarajucu poruku.


from numpy import mean

#jjjjj

arr=[]
#nisam znala da je true, ali posto imam break, logicno je
while True:
    print('Unesi broj')
    number=input()
    if number == "Done":
        break
    try:
        number=float(number)
       #kada upisem broj pa slovo ne zanemaruje mi slovo vec mi baca gresku: zato mora biti u try
        arr.append(number)
    except:
        print("Unesena vrijednost nije broj niti Done")

print('Lista sadrzi: ', len(arr), 'elemenata')
print('min:', min(arr), '\nmax:', max(arr))
print('srednja vrijednost:', mean(arr))
arr.sort()
print(arr)

