#Napišite Python skriptu koja ce ucitati tekstualnu datoteku naziva SMSSpamCollection.txt
# Ova datoteka sadrži 5574 SMS poruka pri cemu su neke oznacene kao spam, a neke kao ham
#a) Izracunajte koliki je prosjecan broj rijeci u SMS porukama koje su tipa ham, a koliko je 
#prosjecan broj rijeci u porukama koje su tipa spam. 
#b) Koliko SMS poruka koje su tipa spam završava usklicnikom ?

data=open('SMSSpamCollection.txt',encoding="utf8")
lines_ham=0
lines_spam=0
count_words_hamm=0
count_words_spam=0
count_usklicnik=0

for line in data:
       line = line.strip()
       line = line.lower()
       words = line.split()

       #for word in words:-ne moze jer to ide rijec po rijec
       if words[0]=="ham":
            #mora ici -1 jer ne zelim da se broji hamm i spam
            count_words_hamm= count_words_hamm + (len(words)-1)
            lines_ham=lines_ham+1
       elif words[0]=="spam":
            count_words_spam= count_words_spam + (len(words)-1)
            lines_spam=lines_spam+1
            #pise da poruke moraju zavrsavati !, znaci to je zadnja rijec
            if words[-1].endswith('!'):
                    count_usklicnik=count_usklicnik + 1
        
print('Prosjecan broj rijeci u porukama koje su tipa ham je', count_words_hamm/lines_ham)
print('Prosjecan broj rijeci u porukama koje su tipa spam je', count_words_spam/lines_spam)
print('Broj rijeci koji zavrsava s usklicnikomu spam porukama:',count_usklicnik)
