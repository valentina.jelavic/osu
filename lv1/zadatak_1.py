#Zadatak 1.4.1 Napišite program koji od korisnika zahtijeva unos radnih sati te koliko je placen 
#po radnom satu. Koristite ugradenu Python metodu input(). Nakon toga izracunajte koliko 
#je korisnik zaradio i ispišite na ekran. Na kraju prepravite rješenje na nacin da ukupni iznos 
#izracunavate u zasebnoj funkciji naziva  total_euro.


print('Unos radnih sati:')
radni_sati=float(input())
print('Koliko ste placeni po radnom satu:')
placa_po_r_satu=float(input())
#workHours = float(input("Unesi broj radnih sati: ")) // mozes i ovako

placa=radni_sati*placa_po_r_satu
print('Zaradili ste :', placa)

#zasebna funkcija:
def racunaj_placu(radni_sati,placa_po_r_satu):
    return radni_sati*placa_po_r_satu

print(racunaj_placu(radni_sati,placa_po_r_satu))


#print(f"{racunaj_placu(radni_sati,placa_po_r_satu)} eura") //f znaci formatirani string