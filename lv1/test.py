#1
def total_euro(workHours,hourPrice):
    return workHours*hourPrice


workHours = float(input("Unesi broj radnih sati: "))
hourPrice = float(input("Unesi placu po radnom satu: "))
print(f"{total_euro(workHours,hourPrice)} eura")


#2
import sys
try:
    number = float(input("Unesi broj između 0 i 1: "))
except ValueError:
    print("Unesena vrijednost nije broj")
    sys.exit(1)
except:
    print("Nesto je poslo po krivu")
    sys.exit(1)
if number < 0.0 or number > 1.0:
    print("Broj nije izmedu 0 i 1")
elif number>=0.9:
    print("Ocjena A")
elif number>=0.8:
    print("Ocjena B")
elif number>=0.7:
    print("Ocjena C")
elif number>=0.6:
    print("Ocjena D")
else:
    print("Ocjena F")

#3
numbers = []
while True:
    number = input("Unesi broj: ")
    if number == "Done":
        break
    try:
        number = float(number)
    except:
        print("Unesena vrijednost nije broj niti Done")
    numbers.append(number)
    

if len(numbers)==0:
    print("Lista je prazna")
else:
    numbers = sorted(numbers)
    print(f"Uneseno brojeva: {len(numbers)}")
    print(f"Sortirana lista: {numbers}")
    print(f"Minimalna vrijednost u listi: {min(numbers)}")
    print(f"Maksimalna vrijednost u listi: {max(numbers)}")
    print(f"Srednja vrijednost: {sum(numbers)/len(numbers)}")

#4
wordsDict = {}

fhand = open ('song.txt')
for line in fhand :
    line = line.rstrip()
    words = line.split()
    for word in words:
        word = word.rstrip(',')
        if word in wordsDict:
            wordsDict[word]+=1
        else:
            wordsDict[word]=1
fhand.close()

print(wordsDict)
count = 0

print("Unique words:")
for key in wordsDict:
    if wordsDict[key]==1:
        print(key)
        count+=1
print(f"Number of unique words: {count}")

#5
hamWordCount = []
spamWordCount = []
exCount = 0
fhand = open ('SMSSpamCollection.txt', encoding='utf-8')

for line in fhand :
    line = line.rstrip()
    words = line.split()
    if words[0]=="ham":
        hamWordCount.append(len(words)-1)
    else:
        spamWordCount.append(len(words)-1)
        if words[-1].endswith('!'):
            exCount+=1
fhand.close()

print(f"Prosjecan broj rijeci u ham porukama: {sum(hamWordCount)/len(hamWordCount)}")
print(f"Prosjecan broj rijeci u spam porukama: {sum(spamWordCount)/len(spamWordCount)}")
print(f"Broj spam poruka koje zavrsavaju usklicnikom: {exCount}")
