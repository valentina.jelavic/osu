import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from keras.models import load_model
from sklearn.metrics import accuracy_score, confusion_matrix, ConfusionMatrixDisplay, classification_report

# Napišite skriptu koja ce ucitati izgradenu mrežu iz zadatka 1 i MNIST skup 
#podataka. Pomocu matplotlib biblioteke potrebno je prikazati nekoliko loše klasificiranih slika iz
#skupa podataka za testiranje. Pri tome u naslov slike napišite stvarnu oznaku i oznaku predvidenu 
#mrežom.

num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)


# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu
X_train_rs = np.reshape(x_train, (60000, 784))
X_test_rs = np.reshape(x_test, (10000, 784))

#NAKON RESHAPEA UCITAMO MODEL-logicno preskocimo ono kad pravimo model u 1.zad
#morala sam spremit negdje random jer mi nece ovako u mapu?
model = load_model('/trecasreca/')

#pravimo predikciju pazi na rs
predictions = model.predict(X_test_rs)

#zato sto uzimamo samo zadnju dimenziju
y_pred = np.argmax(predictions, axis=-1)
y_true = np.argmax(y_test_s, axis=-1)


#uvijet ako su razlicite predicija i stvarni broj na slici
if (y_pred != y_true).any():
    for i in range(1, 4):
        plt.subplots(1, 1)
        #prikazivanje slike na temelju niza piksela
        plt.imshow(x_train[i], cmap=plt.get_cmap('gray'))
        plt.title(f"Predicted: {y_pred[i]} Real: {y_train[i]}")
    plt.show()
