#MNIST podatkovni skup za izgradnju klasifikatora rukom pisanih znamenki
#dostupan je u okviru Keras-a. Skripta zadatak_1.py ucitava MNIST podatkovni skup te podatke
#priprema za ucenje potpuno povezane mreže
import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score, confusion_matrix, ConfusionMatrixDisplay, classification_report

# Model / data parameters
num_classes = 10#broj različitih klasa u skupu podataka, od 0 do 9 rukom pisanih brojeva
input_shape = (28, 28, 1)#dimenzije ulaznih slika u skupu podataka(slika je 28x28)

# train i test podaci, to je minst-direktno iz kerasa
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))#%s predstavlja mjesto gdje ce se umetnuti 
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

# TODO: prikazi nekoliko slika iz train skupa
for i in range(3):
     #y_train je oznaka pa je to 0..9 
     print('Label:', y_train[i])
    #ona je plt.show izvan i dodala subplot
     plt.imshow(x_train[i], cmap=plt.get_cmap('gray'))
     plt.show()


# skaliranje slike na raspon [0,1]
#radi smanjivanja ostecenja
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255


# slike trebaju biti (28, 28, 1)
#np.expand_dims se koristi kako bi se dimenzija slika proširila za jedan dodatni kanal,
#  jer se konvolucijski modeli obično očekuju ulaze oblika (visina, širina, kanali).
x_train_s = np.expand_dims(x_train_s, -1)#-1 za dodavanje nove dimanzije na kraj; da je 0 bilo bi na pocetku
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)# 60000, 28,28,1
print(x_train_s.shape[0], "train samples")#60000
print(x_test_s.shape[0], "test samples")#10000


# pretvori labele
#y_train i y_test u one-hot reprezentaciju pomoću funkcije to_categorical
#primjeti kako se izlazne transformiraju
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)


# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu
#2d u 1 d koji odgovara ulazu u potpuno povezani sloj (dense layer) neuronske mreže
#nije koristila X_s
X_train_rs = np.reshape(x_train, (60000, 784))#to je broj x_traina koji sumo prethodno ispisivali
X_test_rs = np.reshape(x_test, (10000, 784))

model = keras.Sequential()
model.add(layers.Input(shape=(784,)))#28x28 je 
model.add(layers.Dense(100, activation='relu'))#primjer broja neurona i aktivacijske funkcije
model.add(layers.Dense(50, activation='relu'))
model.add(layers.Dense(10, activation ='softmax'))#inace je zadnja softmax

model.summary()#za prikaz strukture


# TODO: definiraj karakteristike procesa ucenja pomocu .compile()
#konfigurira model za proces ucenja
#(odabir optimizacijskog postupka, funkcije gubitka i liste metrika na temelju kojih ce se evaluirati model tijekom ucenja i testiranja
#optimizer za minimizaciju funkcije gubitka, da se dobiju optimalni param
model.compile( loss = "categorical_crossentropy" ,
               optimizer = "adam" ,
               metrics = [ "accuracy" ,])


# TODO: provedi ucenje mreze
batch_size = 32
epochs = 3
#primjeti kako se koriste X_rs i y_s
history = model.fit( X_train_rs, y_train_s,
                     batch_size = batch_size,
                     epochs = epochs,
                     #za validacijske podatke
                     validation_split = 0.1 )



# TODO: Prikazi test accuracy i matricu zabune
#opet Xrs i ys
#verbose je detaljnost ispisa
#evaluacija-gubitak, tocnost..
score = model.evaluate( X_test_rs , y_test_s , verbose = 0 )
print(score) #score[0] funkcija gubitka, score[1] funkcija dobitka

#predikcije
predictions = model.predict(X_test_rs)
print(predictions)


# TODO: Prikazi test accuracy i matricu zabune
#točnost je druga vrijednost u listi pa se može dohvatiti indeksom 1 
print("Tocnost: ", score[1])

# matrica zabune
#axis=-1, argmax() će se primijeniti na posljednju dimenziju tenzora,
# što je u ovom slučaju dimenzija koja označava klase modela
#primjeti da uzimamo iz predicta, nije klasicno .predict i tjt-jer moramo uzeti zadnju dimenziju
y_pred = np.argmax(predictions, axis=-1)
y_true = np.argmax(y_test_s, axis=-1)
cm = confusion_matrix(y_true, y_pred)
print("Matrica zabune: ", cm)
disp = ConfusionMatrixDisplay(cm)
disp.plot()
plt.show()


#NECE MI SPREMIT
# TODO: spremi model


model.save('/trecasreca/')
del model