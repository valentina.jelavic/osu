#Napišite skriptu koja ce ucitati izgradenu mrežu iz zadatka 1. Nadalje, skripta 
#treba ucitati sliku  test.png sa diska. Dodajte u skriptu kod koji ce prilagoditi sliku za mrežu, 
#klasificirati sliku pomocu izgradene mreže te ispisati rezultat u terminal. Promijenite sliku 
#pomocu nekog grafickog alata (npr. pomocu Windows Paint-a nacrtajte broj 2) i ponovo pokrenite 
#skriptu. Komentirajte dobivene rezultate za razlicite napisane znamenke

import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt


from sklearn.metrics import confusion_matrix
from keras.models import load_model
from PIL import Image
from sklearn.metrics import accuracy_score, confusion_matrix, ConfusionMatrixDisplay, classification_report

#ucitavanje modela
model = load_model('/trecasreca/')

#ucitavanje slike
#L je za grayscale
img = Image.open('/trecasreca/test.png').convert("L")
#skalira se na dimenziju 28 puta 28 kao slika koju smo koristili prethodno
img = img.resize((28, 28))

#numpy niz koji sadrži vrijednosti piksela slike
img_array = np.array(img)
#klasicna pretvaranja, ali bez y
img_array = img_array.astype("float32") / 255
img_array = np.expand_dims(img_array, -1)
#1 slika veličine 28x28 pretvorila u vektor duljine 784
img_array = np.reshape(img_array, (1, 784))

#prikaz slike
plt.figure()
plt.imshow(img)
plt.show()
#predikcija kao i tamo
#jel dobije da je predikcija 9
prediction = model.predict(img_array)
pred = np.argmax(prediction)
print(pred)

#sta znaci ovo u paintu?